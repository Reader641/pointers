#include <iostream>

using namespace std;

void myswap(int * ptr1, int * ptr2)
{
	auto temp = *ptr1;
	*ptr1 = *ptr2;
	*ptr2 = temp;
}


int main()
{
	int a = , b = 0;    

	cout<<"What am I doing wrong ☹ \n\n";

	cout<<"a = "<<a<<", b = "<<b<<endl;
 
	myswap(&a, &b); // mine works.

	cout<<"a = "<<a<<", b = "<<b<<endl;
	return 0;
}
